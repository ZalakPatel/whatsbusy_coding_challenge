''' Time Complexity of the implemented function is O(n) '''

def compression(string):
    
    test = ""

    count = 1

    #Assign the first charater
    test = test + string[0]

    #Iterating through loop and skip the last one
    for i in range(len(string)-1):
        
        if(string[i] == string[i+1]):
            count+=1
        else:
            if(count > 1):

                test += str(count)
            
            test += string[i+1]
            count = 1
            
    #printing the last character
    if(count > 1):
        test += str(count)
    return test


string = input("Enter the String : ") 
compression(string)