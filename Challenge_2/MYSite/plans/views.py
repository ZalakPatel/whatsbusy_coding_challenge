from django.shortcuts import render, get_object_or_404, redirect
from .forms import CustomSignupForm
from django.urls import reverse_lazy
from django.views import generic
from .models import Plan, Customer
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required, user_passes_test
import stripe
from django.http import HttpResponse
import time
from datetime import datetime, timedelta

stripe.api_key = "sk_test_51HGHQuJBOAvqUGTyotolEIfwvNfMLXhDQbaowtUb6eGzMjRDZkGmR4YjwEAVk6AQzzT83ND131CAHkfIWdeEMFnD00jglRB7bI"

def home(request):
    plans = Plan.objects
    return render(request, 'plans/home.html', {'plans':plans})

'''Home View'''

def plan(request,pk):
    plan = get_object_or_404(Plan, pk=pk)
    if plan.premium :
        if request.user.is_authenticated:
            try:
                if request.user.customer.membership:
                    return render(request, 'plans/plan.html', {'plan':plan})
            except Customer.DoesNotExist:
                    return redirect('join')
        return redirect('join')
    else:
        return render(request, 'plans/plan.html', {'plan':plan})

'''Plan function to choose from two options:Trial 7 Days OR Monthly'''

def join(request):
    return render(request, 'plans/join.html')

''' Display the join form if we recieve Get Request'''

@login_required
def checkout(request):

    try:
        if request.user.customer.membership:
            return redirect('settings')
    except Customer.DoesNotExist:
        pass
    '''Check if user has a Subscription '''

    price = 49.99
    plan = 'price_1HGdLWJBOAvqUGTyZFDzISDF'
    '''Assigning the Plan API Key'''

    if request.method == 'POST':
        trial_end = datetime.now() + timedelta(days=7)
        trial_end = int(time.mktime(trial_end.timetuple()))    
        
        stripe_customer = stripe.Customer.create(email=request.user.email, source=request.POST['stripeToken'])
        '''Creating Stripe Token'''
        
        subscription = stripe.Subscription.create(
                    customer=stripe_customer.id,
                    items=[{'plan': plan}],
                    trial_end=trial_end,)
        '''Using stripe token to create subsciption and setting a counter to calculate number of days'''

        customer = Customer()
        customer.user = request.user
        customer.stripeid = stripe_customer.id
        customer.membership = True
        customer.cancel_at_period_end = False
        customer.stripe_subscription_id = subscription.id
        customer.save()

    return render(request, 'plans/checkout.html',
        {'plan':plan,'price':price})
    '''Upon checkout with the selected plan return the name of plan and the price'''

def settings(request):
    membership = False
    cancel_at_period_end = False
    '''Initially Set the values to false'''
    
    if request.method == 'POST':
        subscription = stripe.Subscription.retrieve(request.user.customer.stripe_subscription_id)
        subscription.cancel_at_period_end = True
        request.user.customer.cancel_at_period_end = True
        cancel_at_period_end = True
        subscription.save()
        request.user.customer.save()
    else:
        try:
            if request.user.customer.membership:
                membership = True
            if request.user.customer.cancel_at_period_end:
                cancel_at_period_end = True
        except Customer.DoesNotExist:
            membership = False

    return render(request, 'registration/settings.html', {'membership':membership,
    'cancel_at_period_end':cancel_at_period_end})

@user_passes_test(lambda u: u.is_superuser)
def updateaccounts(request):
    customers = Customer.objects.all()
    '''Adding data to Customer object from queryset'''
    for customer in customers:
        subscription = stripe.Subscription.retrieve(customer.stripe_subscription_id)
        '''Retrieves the subscription with the given ID.'''
        if subscription.status != 'active':
            customer.membership = False
        else:
            customer.membership = True
        customer.cancel_at_period_end = subscription.cancel_at_period_end
        '''cancel the subscription at the end of the period/immediately'''
        customer.save()
    return HttpResponse('completed')

class SignUp(generic.CreateView):
    form_class = CustomSignupForm
    success_url = reverse_lazy('home')
    template_name = 'registration/signup.html'

    def form_valid(self, form):
        valid = super(SignUp, self).form_valid(form)
        username, password = form.cleaned_data.get('username'), form.cleaned_data.get('password1')
        new_user = authenticate(username=username, password=password)
        login(self.request, new_user)
        return valid
