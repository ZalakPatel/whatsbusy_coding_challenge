from django.contrib import admin

from .models import Plan, Customer

admin.site.register(Plan)
admin.site.register(Customer)
